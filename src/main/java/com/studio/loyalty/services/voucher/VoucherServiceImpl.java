package com.studio.loyalty.services.voucher;

import com.studio.loyalty.dtos.ReedemDto;
import com.studio.loyalty.dtos.VoucherDto;
import com.studio.loyalty.entities.MerchantEntity;
import com.studio.loyalty.entities.UserEntity;
import com.studio.loyalty.entities.VoucherEntity;
import com.studio.loyalty.repositories.MerchantRepository;
import com.studio.loyalty.repositories.UserRepository;
import com.studio.loyalty.repositories.VoucherRepository;
import com.studio.loyalty.utils.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.sql.SQLException;
import java.util.List;

import static com.studio.loyalty.utils.LogUtils.LOGGER;

@Service
public class VoucherServiceImpl implements VoucherService {

    @Autowired
    VoucherRepository voucherRepository;

    @Autowired
    MerchantRepository merchantRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserUtils userUtils;

    @Override
    public List<VoucherEntity> getAll() {
        return voucherRepository.findAll();
    }

    @Override
    public VoucherEntity getOne(String id) {
        return voucherRepository.findById(id).orElseThrow(() -> new RuntimeException("Data tidak ditemukan"));
    }

    @Override
    public VoucherEntity save(VoucherDto voucher) throws Exception {
        try {
            MerchantEntity merchant = merchantRepository.findById(voucher.getVouchermerchant()).orElseThrow(() -> new RuntimeException("Voucher not found"));
            VoucherEntity u = new VoucherEntity();
            u.setVouchername(voucher.getVouchername());
            u.setVoucherpoint(voucher.getVoucherpoint());
            u.setVouchercode(voucher.getVouchercode());
            u.setMerchant(merchant);
            return voucherRepository.save(u);
        } catch (Exception e) {
            throw new SQLException("Data sudah terdaftar " + e.getMessage());
        }
    }

    @Override
    public VoucherEntity update(VoucherDto voucher, String id) {
        // update belum berhasil
        VoucherEntity u = voucherRepository.findById(id).orElseThrow(() -> new RuntimeException("Voucher not found"));
        u.setVouchername(voucher.getVouchername());
        u.setVoucherpoint(voucher.getVoucherpoint());
        u.setVouchercode(voucher.getVouchercode());
        return voucherRepository.save(u);
    }

    @Override
    public Object delete(String id) throws Exception {
        try {
            voucherRepository.deleteById(id);
            return "Berhasil menghapus data";
        } catch (Exception e) {
            throw new SQLException("Data tidak ditemukan");
        }
    }

    @Override
    public VoucherEntity reedem(ReedemDto reedem){
        UserDetails user = userUtils.auth();
        LOGGER.info(user.toString());
        UserEntity u = userRepository.findByEmail(user.getUsername()).orElseThrow(() -> new RuntimeException("User not found"));
        VoucherEntity v = voucherRepository.findById(reedem.getVoucherid()).orElseThrow(() -> new RuntimeException("Voucher not found"));

        // kurangi point user dengan membeli voucher
        Integer currentPoint = u.getPoint();
        Integer voucherPrice = v.getVoucherpoint();
        Integer newPoint = currentPoint - voucherPrice;
        if (currentPoint < voucherPrice){
            throw new RuntimeException("Point Anda Kurang, Silahkan Melakukan Transaksi Terlebih Dahulu");
        }

        // update data voucher dan point pada user
        u.getVouchers().add(v);
        u.setPoint(newPoint);
        userRepository.save(u);
        return v;
    }
}
