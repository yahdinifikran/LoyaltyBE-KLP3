package com.studio.loyalty.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.studio.loyalty.controllers.*;
import com.studio.loyalty.services.auth.AuthServiceImpl;
import com.studio.loyalty.services.benefit.BenefitServiceImpl;
import com.studio.loyalty.services.merchant.MerchantServiceImpl;
import com.studio.loyalty.services.product.ProductServiceImpl;
import com.studio.loyalty.services.rank.RankServiceImpl;
import com.studio.loyalty.services.role.RoleServiceImpl;
import com.studio.loyalty.services.transaction.TransactionServiceImpl;
import com.studio.loyalty.services.user.UserServiceImpl;
import com.studio.loyalty.services.voucher.VoucherServiceImpl;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest
public class BaseControllerTests {

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @InjectMocks
    protected AuthController authController;

    @InjectMocks
    protected BaseController baseController;

    @InjectMocks
    protected BenefitController benefitController;

    @InjectMocks
    protected MerchantController merchantController;

    @InjectMocks
    protected ProductController productController;

    @InjectMocks
    protected RankController rankController;

    @InjectMocks
    protected RoleController roleController;

    @InjectMocks
    protected TransactionController transactionController;

    @InjectMocks
    protected UserController userController;

    @InjectMocks
    protected VoucherController voucherController;

    @MockBean
    protected AuthServiceImpl authService;

    @MockBean
    protected BenefitServiceImpl benefitService;

    @MockBean
    protected MerchantServiceImpl merchantService;

    @MockBean
    protected ProductServiceImpl productService;

    @MockBean
    protected RankServiceImpl rankService;

    @MockBean
    protected RoleServiceImpl roleService;

    @MockBean
    protected TransactionServiceImpl transactionService;

    @MockBean
    protected UserServiceImpl userService;

    @MockBean
    protected VoucherServiceImpl voucherService;

}
