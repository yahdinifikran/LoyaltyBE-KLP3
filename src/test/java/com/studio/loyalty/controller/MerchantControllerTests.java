package com.studio.loyalty.controller;

import com.studio.loyalty.dtos.BenefitDto;
import com.studio.loyalty.dtos.MerchantDto;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
public class MerchantControllerTests extends BaseControllerTests{

    @Test
    void testMerchantTrue() throws Exception {

        MerchantDto merchantDto = new MerchantDto();

        mockMvc.perform(post("/merchant")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(merchantDto))
                ).andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    void testGetMerchantTrue() throws Exception {

        MerchantDto merchantDto = new MerchantDto();

        mockMvc.perform(get("/merchant")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(merchantDto))
                ).andExpect(status().isOk())
                .andDo(print());
    }


}
