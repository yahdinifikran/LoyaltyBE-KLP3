package com.studio.loyalty.controller;

import com.studio.loyalty.dtos.BenefitDto;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@SpringBootTest
public class BenefitControllerTests extends  BaseControllerTests{

    @Test
    void testBenefitTrue() throws Exception {

        BenefitDto benefitDto = new BenefitDto();

        mockMvc.perform(post("/benefit")
                        .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(benefitDto))
        ).andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    void testGetBenefitTrue() throws Exception {

        BenefitDto benefitDto = new BenefitDto();

        mockMvc.perform(get("/benefit")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(benefitDto))
                ).andExpect(status().isOk())
                .andDo(print());
    }

}
